/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief
 * @file dodedodo.h
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    Marc Hulscher
 * @date      Jun 14, 2013
 * @project   Dodedodo
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
  */

#ifndef DODEDODO_H_
#define DODEDODO_H_

#include <boost/property_tree/ptree.hpp>		//JSON
#include <boost/property_tree/json_parser.hpp>	//JSON
#include <fstream>
#include <iostream>
//#include <stdexcept>
//#include <sstream>
#include <string>
#include <unordered_map>	//possible due to addition of -std=c++0x flag
#include <vector>

#include "active_module.h"
#include "module.h"
#include "moduleconnection.h"
#include "moduleport.h"
#include "line_split.h"
#include "zeromq.h"

class ModuleManager {
private:
	std::unordered_map<ModuleKey, ActiveModule> mActiveModuleMap; // Only used for activated modules (map key is modulekey)
	std::unordered_map<std::string, Module> mUnactiveModuleMap;	// Used to list unactive/available modules (map key = mModuleName)
																// TODO: use full modulename for map key (vliedel/aim_modules/etc.)
	std::unordered_map<std::string,ModuleConnection> mActiveConnections; // (map key = full port name(nb. device.modulename.id.portname))
	// A connection is inserted twice into the mActiveConnections list. Once for each end of the connection
	bool mDebug;
	bool mYarpServRunning;
	bool mZMQServRunning;
	std::string mResource;		// the device name attributed to this dodedodo application (part of the JID)
	std::string mDodedodoID;
	zmqports *myZMQports;
	std::string mRegistryLocation;
	std::string mAimModulesLocation;

public:
	ModuleManager();
	~ModuleManager();
	void Init(std::string resource,bool debug);

	/*	=============================
	 *	mUnactiveModuleMap functions
	 */
	void loadModuleMap();
	void readJsonFile(std::string location);
	void addToUnactive(Module module);
	bool isPresent(std::string moduleName);			// checks if module is installed
	Module & getUnactive(std::string moduleName);
	bool hasPort(std::string moduleName, std::string portName);

	/*	==========================
	 *	mActiveModuleMap functions
	 */
	void addToActive(Module * module,int id);		//TODO: Might make this a boolean
	bool isActive(ModuleKey key);					// checks if moduleKey is in active list
	ActiveModule & getActiveModule(ModuleKey key);	//TODO: remove this function? (not used)

	/* ============================
	 * mActiveConnections functions
	 */
	std::string makeFullPortName(std::string deviceName, ModuleKey key, std::string portName);
	std::string makeFullPortName(ModuleKey key, std::string portName);
	bool verifyPortDataType(std::string fullPortName,std::string type);		// TODO move this to moduleconnection.h/cpp
	bool isConnected(std::string fullPortName);
	bool removeConnection(std::string fullPortName);
	bool isInternal(ModulePort port);					// checks if the port belong to a local module

	/*	==================
	 * 	AIMtools functions
	 */
//	bool aimrun(Module & module,int id);		// id assigned by the server
	bool aimrun(std::string moduleName, int id);// id assigned by the server
	bool aimstop(const ModuleKey &key);			// stops specific module instance
	bool aimstop(std::string moduleName);		// stops all instances of module with moduleName or "all"
	bool aimconnect(ModuleKey key1, ModuleKey key2, std::string portName1, std::string portName2,
			std::string device1, std::string device2);
	bool aimdisconnect(std::string fullPortName);		// TODO: finish
	void aimdataToLocal(ModuleKey key,ModulePort port,int & data);
	void aimdataToLocal(ModuleKey key,ModulePort port,std::vector<int> & data);
	void aimdataToLocal(ModuleKey key,ModulePort port,float & data);
	void aimdataToLocal(ModuleKey key,ModulePort port,std::vector<float> & data);
	void aimdataToLocal(ModuleKey key,ModulePort port,std::string & data);

//	void aimdataToLocal(std::string fullPortName,int& data);
//	void aimdataToLocal(std::string fullPortName,std::vector<int>& data);
//	void aimdataToLocal(std::string fullPortName,float& data);
//	void aimdataToLocal(std::string fullPortName,std::vector<float>& data);
//	void aimdataToLocal(std::string fullPortName,std::string& data);

	std::vector<std::string> aimdataFromLocal();

	boost::property_tree::ptree aimstatus(std::string moduleName);
	boost::property_tree::ptree aimstatus(ModuleKey key);
	boost::property_tree::ptree aimstatus(ModuleKey key,ModulePort port);

	void aimdeploy(Module & module);	// download the module from repo
//	void aimuninstall(Module module);
//	void aimlist();

	/*	====================
	 *	NameServer Functions
	 */
	void startYarpServer();
	void stopYarpServer();
	void startZMQServer();
	void stopZMQServer();
	void ZMQInit();

	friend std::ostream& operator<<(std::ostream& os, const ModuleManager& xec);
};



#endif /* DODEDODO_H_ */
