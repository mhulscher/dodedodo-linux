/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file zeromq.h
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Nov 15, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#ifndef ZEROMQ_H_
#define ZEROMQ_H_

#include <string>
#include <vector>
#include <vector>
#include <string>
#include <sstream>
#include <unistd.h>
// zeromq specific headers
#include <zmq.hpp>
#include <boost/property_tree/ptree.hpp>		//JSON
#include <boost/property_tree/json_parser.hpp>	//JSON
#include <pthread.h>

struct Param {
	std::string server_name;
	std::string server_id;
};

typedef std::vector<int> long_seq;

/**
 * Port name service record. This is like a domain name service record, but instead of containing an IP address and an
 * URI, it comes with a "name" that can be resolved as a "host", "port", and "pid". The name is something like "/write",
 * the host something like "127.0.0.1" or "dev.almende.com" (that is resolvable by dns), "port" is a TCP/UDP port, and
 * "pid" is the process identifier.
 */
typedef struct pns_record_t {
	std::string name;
	std::string host;
	std::string port;
	std::string pid;
} pns_record;

// Following structure makes it easier to store state information per socket
typedef struct zmq_socket_ext_t {
  zmq::socket_t *sock;
  std::string name;
  std::string direction;
  bool ready;
} zmq_socket_ext;

class zmqports {
private:
	// parameter that stores information about the XMPP server
	Param *cliParam;

	// the socket over which is communicated with the name server
	zmq::socket_t *ns_socket;

	// standard control socket over which commands arrive to connect to some port for example
	zmq::socket_t *cmd_socket;

	pthread_t cmdThread;
	pthread_mutex_t cmdMutex;

	// sockets that are used to connect to AIM modules
	std::vector<zmq_socket_ext*> zmq_sockets;

	static void* readCommandsHelper(void* object) {
		((zmqports*)object)->readCommands();
		return NULL;
	}

	void readCommands();
protected:
	static const int channel_count = 2;
	const char* channel[2];

	// the standard zeromq context object
	zmq::context_t *context;
	// some default debug parameter
	bool mDebug;
	/**
	 * The resolve function can be called by modules to get a new socket (and if you want host name and port). It can also
	 * be used by the connector, to bind to these previously set up sockets.
	 */
	void Resolve(pns_record & record);
	void SendAck(zmq::socket_t *s, bool state);
	bool ReceiveAck(zmq::socket_t *s, bool & state, bool blocking);
	char* GetReply(zmq::socket_t *s, bool & state, bool blocking, int & reply_size);
	void SendRequest	(zmq::socket_t *s, bool & state, bool blocking, std::string str);
	void HandleCommand();
	void Connect(std::string source, std::string target);
	zmq::socket_t* GetSocket(std::string name);

public:
	// Default constructor
	zmqports();
	// Default destructor
	virtual ~zmqports();
	// Extend this with your own code, first call zmqports::Init(name);
	void Init(std::string& name,bool debug);
	// Function to get Param struct (to subsequently set CLI parameters)
	inline Param *GetParam() { return cliParam; }

	// add a new port on-the-fly
	void addPort(std::string zmqPortName,std::string direction);

	// remove a port on-the-fly
	void removePort(std::string zmqPortName);

   /**
	* The "readIncoming" function receives stuff over a zeromq REP socket. It works as a client. It is better not
	* to run it in blocking mode, because this would make it impossible to receive message on other ports (under which
	* the /pid/control port). The function returns NULL if there is no new item available.
	*/
	// Read from this function and assume it means something. If you're expecting a float or integer array, use the
	// conversion functions to correctly do this
	std::vector<std::string> listIncomingPorts();
	std::string readIncoming(std::string portname, bool blocking=false);
	long_seq to_long_seq(std::string &data);			//TODO: Not checked yet
	std::vector<float> to_float_seq(std::string &data);	//TODO: Not checked yet


  /**
   * The "writeOutGoing" function takes data that is sent over XMPP and sends it to a local module that is connected
   * to the Dodedodo server. It cannot be blocking because this
   * would make it impossible to receive message on other ports (among which the /pid/control port). It could have been
   * blocking if it is known if it is connected to a REP port (but the connected() function is apparently not meant for
   * that).
   */
  // Write to this function and assume it ends up at some receiving module
	bool writeOutGoing(std::string portname, const int &output);
	bool writeOutGoing(std::string portname, const long_seq &output);
	bool writeOutGoing(std::string portname, const float &output);
	bool writeOutGoing(std::string portname, const std::vector<float> &output);
	bool writeOutGoing(std::string portname, const std::string &output);

};

#endif /* ZEROMQ_H_ */
