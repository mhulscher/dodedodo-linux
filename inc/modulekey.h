/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file modulekey.h
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Sep 16, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#ifndef MODULEKEY_H_
#define MODULEKEY_H_

#include <iostream>
#include <string>

class ModuleKey {
		std::string	mModuleName;
		int	mModuleID;
	public:
		ModuleKey(): mModuleName(""), mModuleID(-1){ }
		ModuleKey(std::string name): mModuleName(name), mModuleID(-1){ }
		ModuleKey(std::string name,int id): mModuleName(name), mModuleID(id) { }
		~ModuleKey(){ }

		std::string getName() const {return mModuleName;}
		int getID() const {return mModuleID;}

		void setName(std::string name){mModuleName=name;}
		void setID(int id){mModuleID=id;}

		bool operator==(const ModuleKey &other) const;
		friend std::ostream& operator<<(std::ostream& os, const ModuleKey& key);
};

/*
 * Hash definition required to use ModuleKey as a unordered_map Key
 */
namespace std {
  template <>
  struct hash<ModuleKey>
  {
    std::size_t operator()(const ModuleKey &m) const
    {
      using std::size_t;
      return (std::hash<std::string>()(m.getName()) ^ (std::hash<int>()(m.getID()) << 1));
    }
  };
}

#endif /* MODULEKEY_H_ */
