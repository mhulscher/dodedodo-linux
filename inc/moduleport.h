/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file moduleport.h
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Sep 16, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#ifndef MODULEPORT_H_
#define MODULEPORT_H_

#include <iostream>
#include <string>

//enum Direction_t {OUT,IN};

class ModulePort {
private:
	std::string mPortName;
	std::string mPortDirection;
	std::string mPortType;
	std::string mMiddleware;
public:
	ModulePort();
	ModulePort(std::string name,std::string direction);
	ModulePort(std::string name,std::string direction,std::string type);
	ModulePort(std::string name,std::string direction,std::string type,std::string middleware);

	~ModulePort();
	void setName(std::string name);
	std::string getName() const;
	void setDirection(std::string direction);
	std::string getDirection() const;
	void setType(std::string type);
	std::string getType() const;
	std::string getMiddleware() const;

	bool operator==(const ModulePort &other) const;
	friend std::ostream& operator<<(std::ostream& os, const ModulePort& port);
};

/*
 * Required to use ModulePort as a unordered_map Key TODO: expand the hash to include port type & middleware
 */
namespace std {
  template <>
  struct hash<ModulePort>
  {
    std::size_t operator()(const ModulePort &m) const
    {
      using std::size_t;
      return (std::hash<std::string>()(m.getName()) ^ (std::hash<std::string>()(m.getDirection()) << 1));
    }
  };
}


#endif /* MODULEPORT_H_ */
