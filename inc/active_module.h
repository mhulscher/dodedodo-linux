/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief ...
 * @file active_module.h
 * 
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Anne C. van Rossum <anne@almende.org>
 *
 * @author    Anne C. van Rossum
 * @date      Oct 23, 2013
 * @project   Replicator 
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      Clustering
 */

#ifndef ACTIVE_MODULE_H_
#define ACTIVE_MODULE_H_

#include <module.h>
#include <modulekey.h>

class ActiveModule {
private:
	const Module *mModule;
	ModuleKey mModuleKey;
//	int mPID;	//TODO: process id linked to running module
public:
	const Module &getModule() { return *mModule; }

	ActiveModule(const Module * module);

	ActiveModule(const Module * module, int id);

	void setKeyID(int id);

	ModuleKey getKey() const;

	friend std::ostream& operator<<(std::ostream& os, const ActiveModule& module);
};



#endif /* ACTIVE_MODULE_H_ */
