#!/bin/make

build_folder = build

all:
	@mkdir -p $(build_folder)
	cd $(build_folder) && cmake $(CMAKE_FLAGS) ../
	cd $(build_folder) && make

clean:
	cd $(build_folder) && make clean
	rm -f $(build_folder)/CMakeCache.txt

install:
	cd $(build_folder) && make install
	cd scripts && make install	

