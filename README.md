# Dodedodo Linux

## What does it do?

The Dodedodo app acts as a manager for AIM modules on a linux machine such as the Raspberry Pi. It receives commands from the Dodedodo server over an XMPP connection.
These commands are then parsed by the app and sent to the modules running on the Pi.

## Compiling the code on your pi

In order to be able to compile dodedodo, you first need to install a few dependencies
```sh
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install cmake python gcc g++-4.7 libboost-dev libboost-thread-dev libssl-dev git-core omniidl

# fix bug in boost's json parser
sudo sed -i -e 's/std::make_pair(c.name, Str(b, e))/std::make_pair(c.name, Ptree(Str(b, e)))/' /usr/include/boost/property_tree/detail/json_parser_read.hpp

#install Swiften (this will take a while)
cd ~
git clone git://swift.im/swift
cd swift
nano 3rdParty/Boost/src/boost/detail/endian.hpp
# add:  "#define __LITTLE_ENDIAN__" at the beginnign of the file  

#increasing swap size on the raspberry pi
sudo nano /etc/dphys-swapfile
#change to 512(MB)
sudo reboot 

cd ~/swift
./scons Swiften
./scons SWIFTEN_INSTALLDIR=/usr/local /usr/local
sudo cp ~/swift/3rdParty/Boost/libSwiften_Boost.a /usr/local/lib

cd ~
git clone --recursive https://github.com/dobots/aim.git

#install zeroMQ
cd ~
wget http://download.zeromq.org/zeromq-3.2.4.tar.gz
tar -xf zeromq-3.2.4.tar.gz
cd zeromq-3.2.4/
./configure
make
DESTDIR=/tmp/rasp sudo make install
sudo cp ~/aim/rur-builder/third/zmq.hpp /usr/include/

#install nodejs
cd ~
wget http://nodejs.org/dist/v0.10.22/node-v0.10.22.tar.gz
tar -xf node-v0.10.22.tar.gz
cd node-v0.10.22
./configure
make
sudo make install

#install npm
curl https://npmjs.org/install.sh | sudo sh

#install zmqconnect
#still requires json_spirit_reader
sudo apt-get install libjson-spirit-dev
cd ~/aim/zmqconnect
make

#install the zmqserver
cd ~/aim/zmqserver
npm install -d
cd node_modules/zmq
make

sudo ldconfig

# install AIM
cd ~/aim
sudo make install
cd zmqconnect
sudo make install

#install dodedodo
cd ~
git clone https://bitbucket.org/mhulscher/dodedodo-linux.git
cd dodedodo-linux
make
sudo make install
```

## Copyrights
The copyrights (2013) belong to:

- License: LGPL v.3
- Author: Marc Hulscher
- Almende B.V., http://www.almende.com and DO bots B.V., http://www.dobots.nl
- Rotterdam, The Netherlands

