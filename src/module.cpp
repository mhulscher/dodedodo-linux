/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file modules.cpp
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Sep 6, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#include "module.h"
#include "line_split.h"

Module::Module(): mDeviceName("raspberrypi"), mMiddleWare("standard"), mRegistered(false) {
}

Module::Module(std::string moduleName, std::string moduleLocation){
	mDeviceName = "raspberrypi";
	mModuleName = moduleName;
	mMiddleWare = "standard";
	mModuleLocation = moduleLocation;
}

std::string Module::getName() const{
	return mModuleName;
}

std::string Module::getMiddleWare() const {
	return mMiddleWare;
}

std::unordered_map<std::string, ModulePort> Module::getPortMap() const{
	return mPorts;
}

std::string Module::getGit() const {
	return mGit.mUrl;
}

void Module::setMiddleWare(std::string middleware){
	mMiddleWare = middleware;
}

void Module::setName(std::string name){
	mModuleName = name;
}

void Module::addPort(std::string portName,std::string portDirection){
	mPorts.insert({portName,ModulePort(portName,portDirection,"")});
}

void Module::addPort(std::string portName, std::string portDirection,std::string portType,std::string portMiddleware){
	if (portMiddleware.empty()) portMiddleware = mMiddleWare;
	mPorts.insert({portName,ModulePort(portName,portDirection,portType,portMiddleware)});
}

void Module::addPort(ModulePort port){
	mPorts.insert({port.getName(),port});
}

bool Module::hasPort(std::string portName) const {
	return (mPorts.count(portName)>0) ? true : false;
}

ModulePort Module::getPort(std::string portName) const {
	return mPorts.at(portName);
}

bool Module::read(boost::property_tree::ptree & pt) {
	bool success = true;

	boost::optional<std::string> name = pt.get_optional<std::string>("name");
	if (!name) success = false;
	boost::optional<std::string> type = pt.get_optional<std::string>("type");
	if (!type) success = false;
	boost::optional<std::string> description = pt.get_optional<std::string>("description");
//	if (!description) success = false;
	boost::optional<bool>enable = pt.get_optional<bool>("enable");
//	if (!enable) success = false; // enable can be absent, then it will be enabled by default

	boost::optional<std::string> git_url = pt.get_optional<std::string>("git");
//	if (!git_url) success = false;
	boost::optional<std::string> android_package = pt.get_optional<std::string>("android.package");
//	if (!android_package) success = false;
	boost::optional<std::string> android_url = pt.get_optional<std::string>("android.url");
//	if (!android_url) success = false;

	// if enable flag exists and is set to false, return
//	if (enable) {
//		 if (!enable.get())
//			 return false;
//	}

	// optional...?
	for (auto & port: pt.get_child("ports")) {
		std::string portMiddleware = "";
		bool port_success = true;
		boost::optional<std::string> pMiddleware = port.second.get_optional<std::string>("middleware");
		if (pMiddleware) portMiddleware = pMiddleware.get();
		boost::optional<std::string> pName = port.second.get_optional<std::string>("name");
		if (!pName) port_success = false;
		boost::optional<std::string> pDirection = port.second.get_optional<std::string>("dir");
		if (!pDirection) port_success = false;
		boost::optional<std::string> pType = port.second.get_optional<std::string>("type");
		if (!pType) port_success = false;

		if (port_success) {
			addPort(pName.get(), pDirection.get(), pType.get(), portMiddleware);
		} else {
			std::cerr << "Port could not be added" << std::endl;
		}
	}

	if (success) {
		mModuleName = name.get();
//		mModuleName = interpretName(mModuleName);
		mType = type.get();
		if(description){mModuleDescription = description.get();}
		if(git_url) {mGit.mUrl = git_url.get();}
		if(android_package){mAndroid.mPackageName = android_package.get();}
		if(android_url){mAndroid.mUrl = android_url.get();}
		if (enable) {
			mEnable = enable.get();
		} else {
			mEnable = true;
		}
	} else {
		std::cerr << "Something went wrong with parsing" << std::endl;
	}

	return success;
}


void Module::write(boost::property_tree::ptree & pt) {
	pt.put<std::string>("name",mModuleName);
	pt.put<std::string>("type",mType);
	pt.put<std::string>("description",mModuleDescription);
	pt.put<bool>("enable",mEnable);
	pt.put<std::string>("git",mGit.mUrl);
	pt.put<std::string>("android.package",mAndroid.mPackageName);
	pt.put<std::string>("android.url",mAndroid.mUrl);
	boost::property_tree::ptree port, ports;
	for (auto& p: mPorts) {
		port.put<std::string>("name",p.second.getName());
		port.put<std::string>("dir",p.second.getDirection());
		port.put<std::string>("type",p.second.getType());
		port.put<std::string>("middleware",p.second.getMiddleware());
		ports.push_back( std::make_pair("",port));
	}
	pt.put_child("ports", ports);
}

std::ostream& operator<<(std::ostream& os, const Module& module){
	os << "\tName:\t" << module.mModuleName << std::endl;				//write the module key
	os << "\tType:\t" << module.mType << std::endl;						//write the module type
	os << "\tDescription:\t" << module.mModuleDescription << std::endl;	//write module description
	os << "\tMiddleware:\t" << module.mMiddleWare << std::endl;			//write which middleware
	os << "\tDevice:\t" << module.mDeviceName << std::endl;				//write device type/name
	os << "\tLocation:\t" << module.mModuleLocation << std::endl;		//write module location
	os << "\tGit:\t" << module.mGit.mUrl << std::endl;					//write git location
	os << "\tAndroid:\t" << std::endl << "\t\tPackage: " << module.mAndroid.mPackageName << std::endl <<
			"\t\tURL: " << module.mAndroid.mUrl << std::endl;
	for (auto& x: module.mPorts) {										//write port contents
		os << "\t" << x.second;
	}
	return os;
}
