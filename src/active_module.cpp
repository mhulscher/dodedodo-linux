/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief ...
 * @file active_module.cpp
 * 
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Anne C. van Rossum <anne@almende.org>
 *
 * @author    Anne C. van Rossum
 * @date      Oct 23, 2013
 * @project   Replicator 
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      Clustering
 */

#include <active_module.h>

ActiveModule::ActiveModule(const Module * module): mModule(module) {
	mModuleKey = ModuleKey(module->getName());	// Key ID is set to '-1' by default. Will be defined when module is activated
}

ActiveModule::ActiveModule(const Module * module, int id): mModule(module) {
	mModuleKey = ModuleKey(module->getName(), id);
}

void ActiveModule::setKeyID(int id){
	mModuleKey.setID(id);
}

ModuleKey ActiveModule::getKey() const {
	return mModuleKey;
}

std::ostream& operator<<(std::ostream& os, const ActiveModule& module){
	os << "\t" << module.mModuleKey;								//write the module key
//	os << "\tDevice:\t" << module.mModule.mDeviceName << std::endl;			//write device type/name
//	os << "\tLocation:\t" << module.mModuleLocation << std::endl;	//write module location
//	for (auto& x: module.mPorts){									//write port contents
//		os << "\t" << x.second;
//	}
	return os;
}

