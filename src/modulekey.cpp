/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file modulekey.cpp
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Sep 16, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#include "modulekey.h"

/*
 *	================================================================
 * 	ModuleKey Functions
 * 	================================================================
 */
bool ModuleKey::operator==(const ModuleKey &other) const {
			return (mModuleName==other.getName() && mModuleID==other.getID());
}

std::ostream& operator<<(std::ostream& os, const ModuleKey& key){
	os << "Modulekey:" << std::endl << "\t\tModuleName: " << key.getName() << std::endl << "\t\tID: " << key.getID() << std::endl;
	return os;
}
