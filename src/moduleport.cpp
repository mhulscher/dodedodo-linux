/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file moduleport.cpp
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Sep 16, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#include "moduleport.h"

/*
 *	================================================================
 * 	ModulePort Functions
 * 	================================================================
 */
ModulePort::ModulePort(){}

ModulePort::ModulePort(std::string name,std::string direction): mPortName(name), mPortDirection(direction) {
	mPortType = "unspecified";
	mMiddleware = "default";
}

ModulePort::ModulePort(std::string name,std::string direction,std::string type): mPortName(name), mPortDirection(direction), mPortType(type){
	mMiddleware = "default";
}

ModulePort::ModulePort(std::string name,std::string direction,std::string type,std::string middleware):
		mPortName(name),
		mPortDirection(direction),
		mPortType(type),
		mMiddleware(middleware){}

ModulePort::~ModulePort(){}

void ModulePort::setName(std::string name){
	mPortName = name;}

std::string ModulePort::getName() const {
	return mPortName;}

void ModulePort::setDirection(std::string direction){
	mPortDirection = direction;}

std::string ModulePort::getDirection() const {
	return mPortDirection;}

void ModulePort::setType(std::string type){
	mPortType = type;}

std::string ModulePort::getType() const {
	return mPortType;}

std::string ModulePort::getMiddleware() const {
	return mMiddleware;
}

bool ModulePort::operator==(const ModulePort &other) const {
			return (mPortName==other.getName() && mPortDirection==other.getDirection() && mPortType==other.getType());
}

std::ostream& operator<<(std::ostream& os, const ModulePort& port){
	os << "Port:\tname:" << port.mPortName << std::endl <<
			"\t\tDirection: " << port.mPortDirection << std::endl <<
			"\t\tType: " << port.mPortType << std::endl <<
			"\t\tMiddleware: " << port.mMiddleware << std::endl;
	return os;
}
