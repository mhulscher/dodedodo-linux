/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief
 * @file xmpp_service.cpp
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    Marc Hulscher
 * @date      Jun 14, 2013
 * @project   Dodedodo
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 */

#include "xmpp_service.h"
#include "line_split.h"

#include <csignal>								//interrupt handling
#include <boost/bind.hpp>
#include <boost/property_tree/ptree.hpp>		//JSON
#include <boost/property_tree/json_parser.hpp>	//JSON
#include <iostream>
#include <sstream>
#include <string>
#include <Swiften/Swiften.h>

using namespace Swift;


int main() {
	std::cout << "Dodedodo app for Raspberry Pi" << std::endl;

	XMPPService startDodedodo;

	return 0;
}

XMPPService::XMPPService(): mDebug(true){

	char* pUsername = getenv("XMPP_NAMESERVER_USERNAME");
	if (pUsername == NULL) {
		std::cerr << "Could not retrieve username from env. variable " << std::endl;
		std::cerr << "please set the variable: XMPP_NAMESERVER_USERNAME to the appropriate value" << std::endl;
		exit(EXIT_FAILURE);
	}
	mUserName = std::string(pUsername);

	char* pPassword = getenv("XMPP_NAMESERVER_PASSWORD");
	if (pPassword == NULL) {
		std::cerr << "Could not retrieve password from env. variable " << std::endl;
		std::cerr << "please set the variable: XMPP_NAMESERVER_PASSWORD to the appropriate value" << std::endl;
		exit(EXIT_FAILURE);
	}
	mPassword = std::string(pPassword);

	char* pResource = getenv("XMPP_NAMESERVER_RESOURCE");
	if (pResource == NULL) {
		std::cerr << "Could not retrieve resource name from env. variable " << std::endl;
		std::cerr << "please set the variable: XMPP_NAMESERVER_RESOURCE to the appropriate value" << std::endl;
		exit(EXIT_FAILURE);
	}
	mResource = std::string(pResource);

	char* pHostName = getenv("XMPP_NAMESERVER_HOST");
	if (pHostName == NULL){
		pHostName = (char*)"dobots.customers.luna.net";
	}
	mHostName = std::string(pHostName);

	char* pAdminJID = getenv("XMPP_ADMIN_USERNAME");
	if (pAdminJID == NULL)
		pAdminJID = (char*)"hal9000@dobots.customers.luna.net";
	mAdminJID = Swift::JID(pAdminJID);

	std::string jid = mUserName + "@" + mHostName + "/" + mResource;
	std::cout << "Logging in as: " << jid << std::endl;
	mJID = Swift::JID(jid);

	mModuleManager.Init(pResource,mDebug);			// initialise the modulemanager

	mNetworkFactories = new Swift::BoostNetworkFactories(&mEventLoop);
	mClient = new Swift::Client(mJID, mPassword, mNetworkFactories);
	mClient->setAlwaysTrustCertificates(); // TODO: this is bad!!

	// bind callback functions
	mClient->onConnected.connect(boost::bind(&XMPPService::handleConnected, this));
	mClient->onPresenceReceived.connect(boost::bind(&XMPPService::handlePresenceReceived, this, _1));
	mClient->onMessageReceived.connect(boost::bind(&XMPPService::handleXMPPMessageReceived, this, _1));
	mClient->getEntityCapsProvider()->onCapsChanged.connect(boost::bind(&XMPPService::handleCapsChanged, this, _1));
	mClient->setSoftwareVersion("Dodedodo","v0.1","raspbian");	//TODO: tailor this to alwyas be correct

	// debugging tool mTracer dumps all XMPP messages to console
	if (mDebug){
		std::cout << mModuleManager << std::endl;
		mTracer = new Swift::ClientXMLTracer(mClient);
	}

//	mSoftwareVersionResponder = new Swift::SoftwareVersionResponder(mClient->getIQRouter());
//	mSoftareVersionResponder->setVersion("Dodedodo", "1.0");
//	mSoftwareVersionResponder->start();

	if(StartInternalThread()){
		std::cout << "internal thread started" << std::endl;
	}

	XMPPConnect();

//	mFileTransferManager = mClient->getFileTransferManager();
}

XMPPService::~XMPPService() {
	delete mNetworkFactories;
	mClient->onConnected.disconnect(boost::bind(&XMPPService::handleConnected, this));
	mClient->onPresenceReceived.disconnect(boost::bind(&XMPPService::handlePresenceReceived, this, _1));
	mClient->onMessageReceived.disconnect(boost::bind(&XMPPService::handleXMPPMessageReceived, this, _1));
	mClient->getEntityCapsProvider()->onCapsChanged.disconnect(boost::bind(&XMPPService::handleCapsChanged, this, _1));
	delete mClient;
	if (mDebug){
		delete mTracer;
	}
//	delete mFileTransferManager;
}

bool XMPPService::XMPPConnect(){
	mClient->connect();
	mEventLoop.run();
	return true;
}

bool XMPPService::XMPPDisconnect(){
	mClient->disconnect();
	return true;
}

bool XMPPService::XMPPSend(JID jid,std::string body){
	if (!mClient->isAvailable()){
		return false;
	} else {
		Swift::Message::ref message (new Swift::Message);
		message->setTo(jid);
		message->setBody(body);
		message->setType(Swift::Message::Chat);
		mClient->sendMessage(message);
		return true;
	}
}

bool XMPPService::XMPPSend(std::string to,std::string body){
	return XMPPSend(Swift::JID(to),body);
}

void XMPPService::handleConnected() {
	if(mDebug){
		std::cout << "XMPP service connected" << std::endl;
	}
	Swift::GetRosterRequest::ref rosterRequest = Swift::GetRosterRequest::create(mClient->getIQRouter());
	rosterRequest->onResponse.connect(boost::bind(&XMPPService::handleRosterReceived, this, _2));
	rosterRequest->send();
}

void XMPPService::handleRosterReceived(Swift::ErrorPayload::ref error) {
	if(mDebug){
		std::cout << "XMPP roster received" << std::endl;
	}
	if (error) {
		std::cerr << "Error receiving roster. Continuing anyway.";
	}
	// Send initial available presence
	mClient->sendPresence(Swift::Presence::create("Online"));

	Swift::Presence::ref subscribe = Swift::Presence::create("Subscribe");
	subscribe->setTo(mAdminJID);
	subscribe->setType(Swift::Presence::Subscribe);
	mClient->sendPresence(subscribe);
}

void XMPPService::handlePresenceReceived(Swift::Presence::ref presence) {
	if(mDebug){
		std::cout << "Presence received, type = " << presence->getType() << std::endl;}
	if (presence->getType() == Swift::Presence::Subscribe) {
		Swift::Presence::ref response = Swift::Presence::create();
		response->setTo(presence->getFrom());
		response->setType(Swift::Presence::Subscribed);
		mClient->sendPresence(response);
	}
}

void XMPPService::handleCapsChanged(Swift::JID jid) {
	std::cout << jid << std::endl;
}

void XMPPService::handleXMPPMessageReceived(Swift::Message::ref message) {
	if(mDebug){std::cout << "XMPP message received..." << std::endl;}
	std::vector<std::string> tokens = split_string(message->getBody());	// TODO: can become a little too large
	if (message->getBody().size() > 0 && tokens[0]=="AIM" && tokens.size()>2 && !(message->getFrom()==mJID) && (message->getTo()==mJID)){
		std::string letdown = "not yet implemented: AIM " + tokens[1];
		std::string invalid = "invalid AIM message";

		//=============================================
		//			AIM data
		//=============================================
		if (tokens[1] == "data" && tokens.size() >= 6){
			// Leave it up to the modules to check if nDim and sizeDN match with array size

			// 		  "AIM" "data" int/float moduleName id port nDim sizeD1 sizeD2 .. data
			//		  "AIM" "data" string 	 moduleName id port some text   here
			// tokens  [0]    [1]	 [2]	   [3]		[4] [5]  [6]  [7]    [8]	[8+]

			std::string name = tokens[3];
//			name = interpretName(name);	// TODO: !!!
			std::string fullPortName=mModuleManager.makeFullPortName(ModuleKey(name,atoi(tokens[4].c_str())),tokens[5]);
			int id = atoi(tokens[4].c_str());
			std::string& port = tokens[5];

			if(tokens[2]=="int"){
				// define int
				int data = atoi(tokens[6].c_str());
				mModuleManager.aimdataToLocal(ModuleKey(name,id), ModulePort(port,"in"), data);

			} else if (tokens[2]=="intarray"){
				// define int vector
				// first value in the array describes nDim (number of dimensions)
				// the following nDim values describe the sizes of dimensions
				// the remaining values describe the data itself
				std::vector<int> data(tokens.size()-6);
				for (unsigned int i=6;i<tokens.size();i++){
					data[i-6] = atoi(tokens[i].c_str());
				}

				mModuleManager.aimdataToLocal(ModuleKey(name, id), ModulePort(port,"in"), data);

			} else if(tokens[2]=="float"){
				// define int
				float data = atof(tokens[6].c_str());
				mModuleManager.aimdataToLocal(ModuleKey(name,id), ModulePort(port,"in"), data);

			} else if (tokens[2]=="floatarray"){
				//define float array
				std::vector<float> data(tokens.size()-6);
				for (unsigned int i=6;i<tokens.size();i++){
					data[i-6] = atof(tokens[i].c_str());
				}
				mModuleManager.aimdataToLocal(ModuleKey(name, id), ModulePort(port,"in"), data);

			} else if (tokens[2]=="string"){
				size_t pos=6; // to account for spaces in between
				for (size_t i=0; i<6; ++i){
					pos += tokens[i].size();
				}
				std::string data = message->getBody().substr(pos);
				mModuleManager.aimdataToLocal(ModuleKey(name, id), ModulePort(port,"in"), data);

			} else{
				if (mDebug)
					XMPPSend(message->getFrom(),invalid); // AIM message is invalid if data type isn't int/float/string
			}
		//=============================================
		//			AIM deploy
		//=============================================
		} else if (tokens[1] == "deploy"){
			//should be provided with module description
			//        "AIM" "deploy" module_description (json msg)
			// tokens  [0]    [1]         [rest]
			std::stringstream ss(message->getBody().substr(11));
			boost::property_tree::ptree pt;
			try{
				boost::property_tree::read_json(ss, pt);
				Module tmp;
				if(tmp.read(pt)){
					if(mDebug){
						std::cout << "successfully received module description, deploying now" << std::endl;
					}
					mModuleManager.aimdeploy(tmp);
				} else {
					if(mDebug){
						std::cout << "something went wrong, try again please" << std::endl;
					}
				}
			} catch(int e) {
				std::cout << "could not read json, error: " << e << std::endl;
			}

		//=============================================
		//			AIM start
		//=============================================
		} else if (tokens[1] == "start" && tokens.size() >= 4 && isdigit(tokens[3].at(0))) {
			//       "AIM" "start" moduleName id
			// tokens [0]    [1]      [2]     [3]
			int id = atoi(tokens[3].c_str());
			if (mModuleManager.aimrun(tokens[2], id)){
				std::cout << "Module started" << std::endl;
			}

		//=============================================
		//			AIM connect
		//=============================================
		} else if (tokens[1] == "connect" && tokens.size()==10) {
			//       "AIM" "connect"  device moduleName id  port_out device moduleName id   port_in
			// tokens [0]      [1]      [2]      [3]    [4]    [5]     [6]      [7]    [8]    [9]
			// the devices [2]&[6] are JID's
			std::string deviceOut = tokens[2];
			if(deviceOut=="local"){
				deviceOut = mJID;
			}
			std::string& nameOut = tokens[3];
//			nameOut = interpretName(nameOut);		// TODO: Remove or alter this later
			int idOut = atoi(tokens[4].c_str());
			std::string& portOut = tokens[5];
			std::string deviceIn = tokens[6];
			if(deviceIn=="local"){
				deviceIn = mJID;
			}
			std::string& nameIn = tokens[7];
//			nameIn = interpretName(nameIn);		// TODO: Remove or alter this later
			int idIn = atoi(tokens[8].c_str());
			std::string& portIn = tokens[9];

			bool success = mModuleManager.aimconnect(ModuleKey(nameOut, idOut),
					ModuleKey(nameIn, idIn), portOut, portIn, deviceOut, deviceIn);

			std::string command_result_pt1 = "AIM command_result connect ";
			unsigned pos = message->getBody().find(tokens[2]);
			std::string command_result_pt2 = message->getBody().substr(pos);
			if(!success){
				XMPPSend(message->getFrom(),command_result_pt1 + "fail " + command_result_pt2);
			} else {
				XMPPSend(message->getFrom(),command_result_pt1 + "ok " + command_result_pt2);
			}

		//=============================================
		//			AIM disconnect
		//=============================================
		} else if (tokens[1] == "disconnect") {
			//       "AIM" "disconnect" moduleName id  portName
			// tokens [0]      [1]        [2]      [3]    [4]
//			std::string name = interpretName(tokens[2]);		// TODO: Remove or alter this later
			std::string fullPortName=mModuleManager.makeFullPortName(ModuleKey(tokens[2],atoi(tokens[3].c_str())),tokens[4]);
			mModuleManager.aimdisconnect(fullPortName);

		//=============================================
		//			AIM stop
		//=============================================
		} else if (tokens[1] == "stop" && tokens.size()>=3) { //&& isdigit(tokens[3].at(0))
			//        "AIM" "stop" moduleName id
			// tokens  [0]    [1]     [2]     [3]
			std::string& name = tokens[2];
//			name = interpretName(name);		// TODO: Remove or alter this later
			if(name=="all"){
				mModuleManager.aimstop(name);
			} else if (tokens.size()>=4){
				mModuleManager.aimstop(ModuleKey(name,atoi(tokens[3].c_str())));
			}

		//=============================================
		//			AIM uninstall
		//=============================================
		} else if (tokens[1] == "uninstall") {
			// 	     "AIM" "uninstall" moduleName
			// tokens [0]      [1]        [2]
			if (mDebug)
				XMPPSend(message->getFrom(), letdown);

		//=============================================
		//			AIM status
		//=============================================
		} else if (tokens[1] == "status" && tokens.size()>=3) {
			// obtaining the status of a single port is not yet supported
			//       "AIM" "status" moduleName id 	portName
			// tokens [0]     [1]       [2]    [3]     [4]
			boost::property_tree::ptree pt;
			std::stringstream ss;

			if (tokens.size() > 2) {
				std::string& name = tokens[2];
//				name = interpretName(name);		// TODO: Remove or alter this later
				if (tokens.size() > 3) {
					int id = atoi(tokens[3].c_str());
					pt = mModuleManager.aimstatus(ModuleKey(name,id));
					if (tokens.size() > 4) {
//						std::string& port = tokens[4];	//TODO
					}
					else
						pt = mModuleManager.aimstatus(ModuleKey(name,id));
				} else
					pt = mModuleManager.aimstatus(name);
			}

			boost::property_tree::json_parser::write_json(ss, pt, true);
			std::string msg = "AIM status_result " + ss.str();
			XMPPSend(message->getFrom(), msg);

		//=============================================
		//	None of the above...(invalid message)
		//=============================================
		} else {
			if (mDebug){
				std::cout << "Invalid AIM Message received " << std::endl;
			}
		}
	} else {
		if (mDebug){
			std::cout << "Invalid AIM command received" << std::endl;
		}
	}
}


void XMPPService::incomingModuleMessages(){
	std::vector<std::string> executable;
	std::vector<std::string> splittable;	/* TODO: right now, the target JID is included in the string.
											 * This should be altered so that we can individually receive the message
											 * and the JID to easily assign them to the message.
											 * Maybe introduce a Message class/struct to do this
											 */
	std::string jid;
	while(true){
		executable.clear();
		executable = this->mModuleManager.aimdataFromLocal();
		for(unsigned i=0;i<executable.size();++i){
			splittable.clear();
			jid.clear();
			splittable=split_string(executable[i]);
			jid = mUserName + "@" + mHostName + "/" + splittable[0];
			XMPPSend(Swift::JID(jid),executable[i].substr(splittable[0].size()+1));	//TODO: the target we send to should be identified and added
		}
		usleep(1000);
	}
}
