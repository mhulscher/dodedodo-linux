/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file zeromq.cpp
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Nov 15, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#include "zeromq.h"

zmqports::zmqports():
  cliParam(0),
  ns_socket(NULL),
  cmd_socket(NULL),
  mDebug(0)
{
	cliParam = new Param();
	context = new zmq::context_t(1);
	pthread_mutex_init(&cmdMutex, NULL);
	pthread_create(&cmdThread, 0, readCommandsHelper, this);
}

zmqports::~zmqports() {
	delete cliParam;
}

void zmqports::Init(std::string & name,bool debug) {
	mDebug = debug;
	cliParam->server_name = "XMPP";
	cliParam->server_id = name;
  if(mDebug){
	  std::cout << "[zeromq] Initialising ZeroMQ ports..." << std::endl;
	  std::cout << "[zeromq] Connecting to name server..." << std::endl;
  }
  ns_socket = new zmq::socket_t(*context, ZMQ_REQ);
  try {
    ns_socket->connect("tcp://127.0.0.1:10101"); // port to connect to, REQ/REP
  } catch (zmq::error_t & e) {
    std::cerr << "[zeromq] Error: Could not connect to the name server: = " << e.what() << std::endl;
  }
  cmd_socket = new zmq::socket_t(*context, ZMQ_REP);

  std::string zmqPortName;
  std::stringstream zmqSs;
  {
    pns_record record;
    zmqSs.clear(); zmqSs.str("");
    zmqSs << getpid();
    record.name = "/resolve/" + zmqSs.str() + "/control";
    record.pid = zmqSs.str();
    Resolve(record);
    zmqSs.clear(); zmqSs.str("");
    zmqSs << "tcp://" << record.host << ":" << record.port;
    std::string zmqPortName = zmqSs.str();
    if(mDebug){
    	std::cout << "[zeromq] Bind to socket " << zmqPortName << std::endl;
    }
    cmd_socket->bind(zmqPortName.c_str());
  }
}

void zmqports::readCommands() {
  while (true) {
    HandleCommand();
  }
}

void zmqports::Resolve(pns_record & record) {
	if(mDebug){
		//std::cout << "[zeromq] Acquire TCP/IP port for " << record.name << ":" << record.pid << std::endl;
		std::cout << "[zeromq] Acquire TCP/IP port for " << record.name << std::endl;
	}
	std::string reqname = record.name + ':' + record.pid;
	zmq::message_t request(reqname.size());
	memcpy((void *)request.data(), reqname.c_str(), reqname.size());
	ns_socket->send(request);

	zmq::message_t reply;
	if (!ns_socket->recv (&reply)) return;
	size_t msg_size = reply.size();
	std::string json = std::string((char*)reply.data(), msg_size);
//	char* address = new char[msg_size+1];
//	memcpy (address, (void *) reply.data(), msg_size);
//	address[msg_size] = '\0';
//	std::string json = std::string(address);
	if(mDebug){
		std::cout << "Received " << json << std::endl;
	}
//	delete [] address;

	// get json property tree
	boost::property_tree::ptree pt;
	std::stringstream ss;
	ss << json;

	try {
		boost::property_tree::read_json(ss,pt);
		record.host = pt.get("server","");
		record.port = pt.get("port","");
		record.pid = pt.get("pid","");
	}
	catch (std::exception &e) {
		std::cerr << "[zeromq] " << "Error: Unable to parse json record: " << e.what() << std::endl;
	}

//	boost::property_tree::basic_ptree<std::string,std::string>::const_iterator iter = pt.begin(),iterEnd = pt.end();
//	for(;iter != iterEnd;++iter){
//	    if (iter->first == "identifier") {
////	       same thing
//	    } else if (iter->first == "server") {
//	      record.host = pt.get<std::string>(iter->first);
//	    } else if (iter->first == "port") {
//	      record.port = pt.get<std::string>(iter->first);
//	    } else if (iter->first == "pid") {
//	      record.pid =pt.get<std::string>(iter->first);
//	    }
//	}
}

void zmqports::SendAck(zmq::socket_t *s, bool state) {
  if (mDebug) std::cout << "[zeromq] Send ACK" << std::endl;
  SendRequest(s, state, true, "ACK");
}

bool zmqports::ReceiveAck(zmq::socket_t *s, bool & state, bool blocking) {
  int reply_size = 0;
  char *reply = GetReply(s, state, blocking, reply_size);
  if (reply == NULL) return false;
  if (reply_size < 1) {
    std::cerr << "[zeromq] Error: Reply is not large enough" << std::endl;
    delete [] reply;
    return false;
  }
  std::string req = std::string(reply, reply_size-1);
  delete [] reply;
  if (req.find("ACK") != std::string::npos) {
    if (mDebug){
    	std::cout << "[zeromq] Got ACK, thanks!" << std::endl;
    }
    return true;
  }
  std::cerr << "[zeromq] Error: got \"" << req << "\", no ACK, state compromised" << std::endl;
  return false;
}

char* zmqports::GetReply(zmq::socket_t *s, bool & state, bool blocking, int & reply_size) {
  if (s == NULL)
    return NULL;
  zmq::message_t reply;
  char* result = NULL;
  reply_size = 0;
  try {
    if (blocking)
      state = s->recv(&reply);
    else
      state = s->recv(&reply, ZMQ_DONTWAIT);
  } catch (zmq::error_t &e) {
    std::cout << "[zeromq] Error: received zmq::error_t " << e.what() << std::endl;
  }
  if (state) {
    size_t msg_size = reply.size();
    result = new char[msg_size+1];
    memcpy(result, (void *) reply.data(), msg_size);
    result[msg_size] = '\0';
    reply_size = msg_size+1;
//	std::cout << "Result: \"" << std::string(result) << "\"" << std::endl;
  }
  return result;
}

void zmqports::SendRequest(zmq::socket_t *s, bool & state, bool blocking, std::string str) {
  if (state) {
    zmq::message_t request(str.size());
    memcpy((void *) request.data(), str.c_str(), str.size());
//	  zmq::message_t request(str.size());
//	  memcpy((void *) request.data(), str.c_str(), str.size());
    if (mDebug){
//    	std::cout << "[zeromq] Send request: " << str << "("<< str.size() <<")"<< std::endl;
    	std::cout << "[zeromq] Send request: " << std::endl;
    }
	if (blocking){
		state = s->send(request);
	} else {
		state = s->send(request, ZMQ_DONTWAIT);
	}
  } else {
    std::cout << "[zeromq] Send nothing (still waiting to receive) " << std::endl;
  }
}

void zmqports::HandleCommand() {
  int reply_size = -1;
  bool state = false;
  char *reply = GetReply(cmd_socket, state, true, reply_size);
  if (reply == NULL) return;
  std::cout << "[zeromq] HandleCommand: " << std::string(reply, reply_size) << std::endl;
  if (reply_size < 3) {
	  std::cerr << "[zeromq] Error: Reply is not large enough for magic header + command string" << std::endl;
	  delete [] reply;
	  return;
  }
  char magic_value = reply[0];
  if (magic_value == 0x01) { // connect to command...
    std::string name = std::string(reply+1, reply_size-2);
    unsigned int pos = name.find("->");
    if (pos == std::string::npos) {
      std::cerr << "[zeromq] Error: no -> separator in connect command" << std::endl;
      delete [] reply;
      return;
    }
    std::string source = name.substr(0, pos);
    std::string target = name.substr(pos+2); // todo:
    if(mDebug){
    	std::cout << "[zeromq] Connect from " << source << " to " << target << std::endl;
    }
    Connect(source, target);
  } else {
    std::cerr << "[zeromq] Error: Unknown command!" << std::endl;
  }
  SendAck(cmd_socket, true);
  delete [] reply;
}

void zmqports::Connect(std::string source, std::string target) {
  pthread_mutex_lock(&cmdMutex);
  zmq::socket_t *s = GetSocket(source);
  if (s == NULL) {
    pthread_mutex_unlock(&cmdMutex);
    return;
  }
  pns_record t_record;
  t_record.name = "/resolve" + target;
  Resolve(t_record);
  std::stringstream ss; ss.clear(); ss.str("");
  ss << "tcp://" << t_record.host << ":" << t_record.port;
  std::string sock = ss.str();
  if(mDebug){
	  std::cout << "[zeromq] Connect to socket " << sock << std::endl;
  }
  try {
    s->connect(sock.c_str());
  } catch (zmq::error_t &e) {
    std::cerr << "[zeromq] Error: Could not connect to " << target << ", because: " << e.what() << std::endl;
  }
  pthread_mutex_unlock(&cmdMutex);
}

zmq::socket_t* zmqports::GetSocket(std::string name) {
	unsigned pos = 0;
	if(name.compare(pos,pos+1,"/")){
		name.substr(pos+1);
	}
	for (unsigned int i = 0; i < zmq_sockets.size(); ++i) {
		if (zmq_sockets[i]->name.find(name) != std::string::npos) return zmq_sockets[i]->sock;
	}
	std::cerr << "[zeromq] Error: socket name could not be found! " << name << std::endl;
	return NULL;
//	assert(false); // todo, get the previously registered socket by name
}

std::vector<std::string> zmqports::listIncomingPorts(){
	std::vector<std::string> portNames;
	for(unsigned i =0;i<zmq_sockets.size();++i){
		if(zmq_sockets[i]->direction=="in"){
			portNames.push_back(zmq_sockets[i]->name);
		}
	}
	return portNames;
}

std::string zmqports::readIncoming(std::string portname,bool blocking) {
	pthread_mutex_lock(&cmdMutex);
	int reply_size = -1;
	zmq_socket_ext portIn;
	// find the port corresponding to the portname
	for(unsigned i=0;i<zmq_sockets.size();++i){
//		if(zmq_sockets[i]->name==("/" + cliParam->server_name + cliParam->server_id + "/" + portname)){
		if(zmq_sockets[i]->name==portname){
			portIn.name=zmq_sockets[i]->name;
			portIn.sock=zmq_sockets[i]->sock;
			portIn.ready=zmq_sockets[i]->ready;
			portIn.direction=zmq_sockets[i]->direction;
		}
	}

	if(portIn.name=="" && mDebug){
		std::cout << "[zeromq] Incoming port not found" << std::endl;
		pthread_mutex_unlock(&cmdMutex);
		return "";
	}
//	} else if(mDebug) {
//		std::cout << "[zeromq] Incoming port found" << std::endl;
//	}

	char *reply = GetReply(portIn.sock, portIn.ready, blocking, reply_size);
	if (!portIn.ready) {
		delete [] reply;
		pthread_mutex_unlock(&cmdMutex);
//		return NULL;
		return "";
	}
	if (reply == NULL) {
		pthread_mutex_unlock(&cmdMutex);
//		return NULL;
		return "";
	}
	SendAck(portIn.sock, portIn.ready);
	pthread_mutex_unlock(&cmdMutex);
	if (reply_size < 2) {
		std::cerr << "[zeromq] Error: Reply is not large enough to store a value!" << std::endl;
		delete [] reply;
		return "";
	}
	std::string input;
	input = std::string(reply, reply_size-1);
	if(mDebug){
		std::cout << "[zeromq] Received string data, size=" << input.size() << std::endl;
	}
	return input;
}

long_seq zmqports::to_long_seq(std::string &data){
	 std::stringstream ss; ss.clear(); ss.str("");
	  ss << data;
	  int itemVal;
	  std::stringstream ssItem;
	  long_seq longSeqIN;
	  longSeqIN.clear();
	  std::string item;
	  char delim(' ');
	  while (std::getline(ss, item, delim)) {
	    ssItem.clear(); ssItem.str("");
	    ssItem << item;
	    ssItem >> itemVal;
	    longSeqIN.push_back(itemVal);
	  }
	  return longSeqIN;
}

std::vector<float> zmqports::to_float_seq(std::string &data){
	std::stringstream ss; ss.clear(); ss.str("");
	ss << data;
	float itemVal;
	std::stringstream ssItem;
	std::vector<float> floatSeqIN;
	floatSeqIN.clear();
	std::string item;
	char delim(' ');
	while (std::getline(ss, item, delim)) {
		ssItem.clear(); ssItem.str("");
	    ssItem << item;
	    ssItem >> itemVal;
	    floatSeqIN.push_back(itemVal);
	  }
	  return floatSeqIN;
}

bool zmqports::writeOutGoing(std::string portname, const int &output){
	std::stringstream ss; ss.clear(); ss.str("");
	ss << output;
	bool valid = writeOutGoing(portname,ss.str());
	return valid;
}

bool zmqports::writeOutGoing(std::string portname, const long_seq &output) {
	std::stringstream ss; ss.clear(); ss.str("");
	if (output.empty())
	  return true;
	long_seq::const_iterator it=output.begin();
	ss << *it++;
	for (; it!= output.end(); ++it)
	  ss << " " << *it;

	bool valid = writeOutGoing(portname,ss.str());

  return valid;
}

bool zmqports::writeOutGoing(std::string portname, const float &output){
	std::stringstream ss; ss.clear(); ss.str("");
	ss << output;
	bool valid = writeOutGoing(portname,ss.str());
	return valid;
}

bool zmqports::writeOutGoing(std::string portname, const std::vector<float> &output) {
	std::stringstream ss; ss.clear(); ss.str("");
	if (output.empty())
		return true;
	std::vector<float>::const_iterator it=output.begin();
	ss << *it++;
	for (; it!= output.end(); ++it)
		ss << " " << *it;

	bool valid = writeOutGoing(portname,ss.str());

  return valid;
}

bool zmqports::writeOutGoing(std::string portname, const std::string &output) {
	std::stringstream ss; ss.clear(); ss.str("");
	zmq_socket_ext portOut;
	ss << output; // very dirty, no endianness, etc, just use the stream operator itself
	pthread_mutex_lock(&cmdMutex);
	// find the port corresponding to the portname
	for(unsigned i=0;i<zmq_sockets.size();++i){
//		std::cout << zmq_sockets[i]->name << std::endl;;
//		if(zmq_sockets[i]->name==("/" + cliParam->server_name + cliParam->server_id + "/" + portname)){
		if(zmq_sockets[i]->name==portname){
			portOut.name=zmq_sockets[i]->name;
			portOut.sock=zmq_sockets[i]->sock;
			portOut.ready=zmq_sockets[i]->ready;
			portOut.direction=zmq_sockets[i]->direction;
		}
	}
	if(portOut.name==""){
//		std::cout << zmq_sockets.size() << std::endl;
		std::cerr << "[zeromq] Error: no correct port found" << std::endl;
		pthread_mutex_unlock(&cmdMutex);
		return false;
	} else if(mDebug){
		std::cout << "[zeromq] Port found! sending data over port: " << portOut.name << std::endl;
	}

	// send data if it's ready and direction matches
	bool state = portOut.ready;
	if(portOut.direction=="out"){
		SendRequest(portOut.sock,state,false,ss.str());
	}
	if(state){
		portOut.ready = false;
	}
	if(!portOut.ready){
		bool ack_state=true;
		ReceiveAck(portOut.sock, ack_state,true);
		if (ack_state) {
			portOut.ready = true;
			pthread_mutex_unlock(&cmdMutex);
			return true;
		}
	}
	pthread_mutex_unlock(&cmdMutex);
	return false;
}

void zmqports::addPort(std::string zmqPortName,std::string direction){
	// create socket object
	zmq_socket_ext* port = new zmq_socket_ext;
	if(direction=="out"){
		port->sock = new zmq::socket_t(*context,ZMQ_REQ);
	}else if(direction == "in"){
		port->sock = new zmq::socket_t(*context,ZMQ_REP);
	} else {
		if(mDebug){
			std::cout << "[zeromq] no correct direction given: " << direction << std::endl;}
	}
	port->direction = direction;
	port->ready = true;
	port->name = "/" + cliParam->server_name + cliParam->server_id + "/" + zmqPortName;

	if(mDebug){
		std::cout << "[zeromq] Adding new zeromq port, named: " << port->name << std::endl;
	}

	// add port to the list of other ports
	zmq_sockets.push_back(port);

	pns_record record;
	record.name = "/resolve" + port->name;
	//	cmd_socket = new zmq::socket_t(*context, ZMQ_REP);
	std::stringstream zmqSs;
	zmqSs.clear(); zmqSs.str("");
	zmqSs << getpid();						// ??
	record.pid = zmqSs.str();
	Resolve(record);
	zmqSs.str("");
	zmqSs << "tcp://" << record.host << ":" << record.port;
	zmqPortName = zmqSs.str();
	if(mDebug){
		std::cout << "[zeromq] Bind to socket: " << zmqPortName << std::endl;
	}
	port->sock->bind(zmqPortName.c_str());
	if(mDebug){
		std::cout << "[zeromq] Zeromq Port " << port->name << " added" << std::endl;
	}
}

void zmqports::removePort(std::string zmqPortName){
	// find the ports position in zmqsockets
	zmq_socket_ext port;
	// find the port corresponding to the portname
	for(unsigned i=0;i<zmq_sockets.size();++i){
		if(zmq_sockets[i]->name==zmqPortName){
//			zmq_sockets[i]->sock.close();
			if(mDebug){
				std::cout << "[zeromq] Removing port: " << zmqPortName << std::endl;
			}
			delete zmq_sockets[i]->sock;
			zmq_sockets.erase(zmq_sockets.begin()+i);
		}
	}

}
