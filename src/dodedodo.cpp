/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief
 * @file dodedodo.cpp
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    Marc Hulscher
 * @date      Jun 14, 2013
 * @project   Dodedodo
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
  */


//#include <sys/stat.h>		//S_ISDIR
//#include <map>
//#include <pthread.h>

#include "active_module.h"
#include "dodedodo.h"
#include "execute_command.hpp"
#include "line_split.h"

ModuleManager::ModuleManager():
mYarpServRunning(false),//assumed not to be running
mZMQServRunning(false)	//assumed to be running
{
	std::string home_folder = getenv("HOME");
	mRegistryLocation = home_folder + "/.rur/data/aim_registry.txt";

	char* pAimModulesLocation = getenv("AIM_WORKSPACE");		// should be derived from the startup script
	if(pAimModulesLocation==NULL){
		mAimModulesLocation = home_folder + "/aim_workspace";
	} else {
		mAimModulesLocation = std::string(pAimModulesLocation);
	}
	myZMQports = new zmqports();
}

ModuleManager::~ModuleManager(){
//	aimstop("all");
}

void ModuleManager::Init(std::string resource,bool debug){
	mResource = resource;
	mDebug = debug;
	loadModuleMap();		// read the registryfile and load all available modules
	mDodedodoID = "";		// define ID for the zeromq ports
	startZMQServer();		// start the zeroMQ name server
	ZMQInit();				// start up the zeroMQ ports of the dodedodo portal
}
// ===================================================================================================================
//						mUnactiveModuleMap functions
// ===================================================================================================================
void ModuleManager::loadModuleMap(){
	std::string line;
	std::vector<std::string> tokens;
	std::ifstream registryFile;
	registryFile.open(mRegistryLocation);
	// read registry file
	if(registryFile.is_open()){
		while(registryFile.good()){
			getline (registryFile,line);
			tokens = split_string(line);
			if (!line.empty() && tokens[1]=="=" && tokens.size()==3){
				readJsonFile(tokens[2]);
			}
		}
	}
}

void ModuleManager::readJsonFile(std::string location){
	std::string jsonlocation = location + "/aim-core/aim_deployment.json";
	std::ifstream file;
	if(mDebug){
		std::cout << "Checking JsonFile: " << jsonlocation << std::endl;
	}

	if(!fileExist(jsonlocation)) return;	//TODO: not tested yet

	boost::property_tree::ptree pt;
	boost::property_tree::read_json(jsonlocation,pt,std::locale());
	Module module;
	bool success = module.read(pt);

	if(success){
		struct stat sb;
		if(stat((location + "/builds/standard").c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)){
			module.setMiddleWare("standard");
		} else if(stat((location + "/builds/yarp").c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)){
			module.setMiddleWare("yarp");
		} if(stat((location + "/builds/ros").c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)){
			module.setMiddleWare("ros");
		} if(stat((location + "/builds/zeromq").c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)){
			module.setMiddleWare("zeromq");
		}
		module.setResource(location);
		addToUnactive(module);
	}
}

void ModuleManager::addToUnactive(Module module){
	mUnactiveModuleMap.insert({module.getName(),module});
}

bool ModuleManager::isPresent(std::string moduleName){
	return (mUnactiveModuleMap.count(moduleName)>0) ? true : false;
}

Module & ModuleManager::getUnactive(std::string moduleName){
        return mUnactiveModuleMap.at(moduleName);
}

bool ModuleManager::hasPort(std::string moduleName,std::string portName){
	if(isPresent(moduleName)){
		Module test = getUnactive(moduleName);
		return test.hasPort(portName);
	}
	return false;
}

// ==================================================================================================================
//					mActiveModuleMap functions
// ==================================================================================================================
void ModuleManager::addToActive(Module * module,int id){
        ActiveModule & active_module = *new ActiveModule(module);
        ModuleKey key = active_module.getKey();
        key.setID(id);
        active_module.setKeyID(id);
        mActiveModuleMap.insert({key,active_module});
}

bool ModuleManager::isActive(ModuleKey key){
        return (mActiveModuleMap.count(key)>0) ? true : false;
}

ActiveModule & ModuleManager::getActiveModule(ModuleKey key){
        return mActiveModuleMap.at(key);
}

// ===================================================================================================================
//					mActiveConnections functions
// ===================================================================================================================
std::string ModuleManager::makeFullPortName(std::string deviceName, ModuleKey key, std::string portName){
	// full port name is of form: device.modulename.id.portname
	// this is also separately defined in moduleconnection::getotherFullPortName, TODO:.. combine these into one to ease changing in the future
	std::string fullPortName = deviceName + "." + key.getName() + "." + std::to_string(key.getID()) + "." + portName;
	return fullPortName;
}

std::string ModuleManager::makeFullPortName(ModuleKey key, std::string portName){
	// uses the local device as device
	return makeFullPortName(mResource,key,portName);
}

bool ModuleManager::verifyPortDataType(std::string fullPortName,std::string type){
	if(isConnected(fullPortName)){
		return (mActiveConnections.at(fullPortName).getPort(fullPortName).getType()==type);
	}
	return false;
}

bool ModuleManager::isConnected(std::string fullPortName){
    return (mActiveConnections.count(fullPortName)>0) ? true : false;
}

bool ModuleManager::removeConnection(std::string fullPortName){
	if(isConnected(fullPortName)){
		std::string otherFullPortName = mActiveConnections.at(fullPortName).getOtherFullPortName(fullPortName);
		if(otherFullPortName==""){
			return false;
		}

		//retrieve both port objects
		ModulePort firstPort = mActiveConnections.at(fullPortName).getSourcePort();
		ModulePort secondPort = mActiveConnections.at(fullPortName).getSinkPort();
//		ModulePort secondPort= mActiveConnections.at(fullPortName).getOtherPort(firstPort);
		// check if either port is a zmqport && remove it
		if(isInternal(firstPort)){
			myZMQports->removePort(firstPort.getName());
		} else if(isInternal(secondPort)){
			myZMQports->removePort(secondPort.getName());
		}
		mActiveConnections.erase(fullPortName);
		mActiveConnections.erase(otherFullPortName);
		return true;
		//TODO: there is no aimdisconnect as of yet, this should be added later.
		//right now, we leave things as is and overwrite existing connections
	}
	return false;
}

bool ModuleManager::isInternal(ModulePort port){
	std::vector<std::string> portname = split_string_by_char(port.getName(),'/');
	if(portname[0]==("XMPP" + mDodedodoID) || portname[1]==("XMPP" + mDodedodoID)){
		return true;
	}
	return false;
}

//=====================================================================================================================
//							AIM tools
//=====================================================================================================================
bool ModuleManager::aimrun(std::string modulename, int id){
	ModuleKey key(modulename, id);
	if(isPresent(modulename)){
	Module& module = getUnactive(modulename);
		if (isActive(key)) {
			std::cerr << "Module " <<  key.getName() << " already active" << std::endl;
			return true;
		} else {
			std::string command = "aimrun " + modulename + " " + std::to_string(id);
			if (execute_command(command)){	// Add to active list if successfully started
				// TODO: should be able to bind the module so I can remove it from the list if stopped externally
				addToActive(&module,id);
				return true;
			} else {
				return false;
			}
		}
	} else {
		std::cout << "Module " << modulename << " not deployed" << std::endl;
		return false;
	}
}
/*
bool ModuleManager::aimrun(Module & module, int id){
	std::cout << "so far so good [1]" << std::endl;
	ModuleKey key(module.getName(), id);
	std::cout << "so far so good [2]" << std::endl;
	if(isPresent(key.getName())){
		std::cout << "so far so good [3]" << std::endl;
		if (isActive(key)) {
			std::cout << "so far so good [4]" << std::endl;
			std::cerr << "Module " <<  key.getName() << " already active" << std::endl;
			return true;
		} else {
			std::string command = "aimrun " + key.getName() + " " + std::to_string(key.getID());
			if (execute_command(command)){	// Add to active list if successfully started
				// TODO: should be able to bind the module so I can remove it from the list if stopped externally
				addToActive(&module,id);
				return true;
			} else {
				return false;
			}
		}
	} else {
		std::cout << "Module " << key.getName() << " not deployed" << std::endl;
		return false;
	}
}
*/

bool ModuleManager::aimstop(const ModuleKey &key){
	//check if the Module is currently active
	if(isActive(key)){
		ActiveModule& module = getActiveModule(key);
		// TODO: disconnect all ports on the Module!!!!!
		std::string command = "aimstop " + module.getModule().getMiddleWare() +
				" "+ key.getName() + " " + std::to_string(key.getID());
		if (execute_command(command)){	//Remove from list if successfully stopped
			mActiveModuleMap.erase(key);
			return true;
		}
	} else if(mDebug){
		std::cerr << "Module " << key.getName() << " is not active" << std::endl;
	}
	return false;
}

bool ModuleManager::aimstop(std::string moduleName){
	for (auto& module: mActiveModuleMap){
		if (module.first.getName()==moduleName || moduleName=="all"){
			aimstop(module.first);
		}
	}
	return true;
}

bool ModuleManager::aimconnect(ModuleKey key1, ModuleKey key2, std::string portName1, std::string portName2,
		std::string device1, std::string device2){

	std::string middleware;
	std::string command;

	device1 = interpretName(device1);	// only look at the last word after the '/', not the full JID
	device2 = interpretName(device2);	// only look at the last word after the '/', not the full JID

	// create mappable portnames to lookup the connection later
	// the form of the map names is: "device.modulename.id.portname" (is defined in only 1 place, n.b. makeFullPortName)
	std::string mapPortName1 = makeFullPortName(device1,key1,portName1);
	std::string mapPortName2 = makeFullPortName(device2,key2,portName2);

	// check if either port is already connected
	if (!isConnected(mapPortName1) && !isConnected(mapPortName2)){
		if(mDebug){
			std::cout << "[aimconnect] connecting module " << key1.getName() << " " << key1.getID() << " to " <<
					key2.getName() << " " << key2.getID() << std::endl;
		}
	} else {
		if(mDebug){
			std::cout << "[aimconnect] one or more ports were already connected" << std::endl;
		}
		removeConnection(mapPortName1);
		removeConnection(mapPortName2); //should already be done by 'removeConnection'
	}

	// check if the connection is supposed to be local
	if(device1==device2 && device1 == mResource){
		if(mDebug){
			std::cout << "[aimconnect] Both modules should be locally present" << std::endl;
		}
		// find the reference to both modules
		if(isActive(key1) && isActive(key2)){
			ActiveModule module1 = getActiveModule(key1);
			ActiveModule module2 = getActiveModule(key2);
			// find the ports we wish to connect
			if(module1.getModule().hasPort(portName1) && module2.getModule().hasPort(portName2)){
				ModulePort port1 = module1.getModule().getPort(portName1);
				ModulePort port2 = module2.getModule().getPort(portName2);
				// check if both middlewares are supposed to be the same
				if(module1.getModule().getMiddleWare()==module2.getModule().getMiddleWare()){
					middleware = mActiveModuleMap.at(key1).getModule().getMiddleWare();
					std::cout << "so far so good [3] " << std::endl;
					//connect the modules (the middlewares are still limited to being either yarp or zeromq)
					command = "aimconnect " + middleware + " " +
							key1.getName() + " " + std::to_string(key1.getID()) + " " + portName1 + " " +
							key2.getName() + " " + std::to_string(key2.getID()) + " " + portName2;
					execute_command(command);

					// insert both ends of the connection into the mActiveConnections list
					ModuleConnection tmp = ModuleConnection(port1,key1,port2,key2,mActiveModuleMap.at(key1).getModule().getMiddleWare());
					mActiveConnections.insert({mapPortName1,tmp});
					mActiveConnections.insert({mapPortName2,tmp});
					return true;
				} else {
					if(mDebug){
						std::cerr << "[aimconnect] modules are not build to the same middleware" << std::endl;
					}
					return false;
				}
			} else {
				if(mDebug){
					std::cerr << "[aimconnect] One or more ports do not belong to their supposed modules" << std::endl;
				}
				return false;
			}
		} else {
			if(mDebug){
				std::cerr << "[aimconnect] One or more modules are not running" << std::endl;
			}
			return false;
		}
	} else if(device1==mResource && device2!=mResource){	// first key is local (create input port to connect to)
		if(mDebug){
			std::cout << "[aimconnect] only one module should be running locally, creating input port for it to connect to" << std::endl;
		}
		middleware = "zeromq";
//		ActiveModule localModule;
		ModulePort localPort;
		if(isActive(key1) && hasPort(key1.getName(),portName1)){
			ActiveModule localModule = mActiveModuleMap.at(key1);
			localPort = localModule.getModule().getPort(portName1);
		} else {
			if(mDebug){
				std::cerr << "[aimconnect] supposed local module" << key1.getName() << "is not running or it does not have the port that is suggested" << std::endl;
			}
			return false;
		}
		//Create new ModulePort that describes the other end
		std::string dataType = localPort.getType();
		ModulePort virtualPort = ModulePort(portName2,"in",dataType,middleware);

		//Create incoming zeromq port
		myZMQports->addPort(mapPortName2,"in");

		// rename virtual port name to comply with zmq form
		std::string newPortName = "/XMPP" + mDodedodoID + "/" + mapPortName2;
		virtualPort.setName(newPortName);
		if(mDebug){
			std::cout << "[aimconnect] new incoming port defined by: " << newPortName << std::endl;
		}

		//Connect zeromq port to local module
		std::string moduleName = key1.getName();
		std::transform(moduleName.begin(),moduleName.end(),moduleName.begin(), ::tolower);
		/* TODO: the interpretName function is now added so the modules can be properly connected.
		 * They announce themselves to the zmqserver without the git/repo/ prefix and thus can't be found
		 * with the entire git/repo/modulename this should be fixed on the level of the zmqserver, zmqconnect
		 *  or default module ports
		 */
		command = "zmqconnect /" + interpretName(moduleName) +	std::to_string(key1.getID()) + "/" + portName1 + " " + newPortName;
		execute_command(command);

		// add to connections list
		ModuleConnection tmp = ModuleConnection(localPort,key1,device1,virtualPort,key2,device2,middleware);
		mActiveConnections.insert({mapPortName1,tmp});
		mActiveConnections.insert({mapPortName2,tmp});
		return true;
	} else if(device2==mResource && device1!=mResource){	// second key is local (create output port to connect to)
		if(mDebug){
			std::cout << "[aimconnect] only one module should be running locally, creating output port for it to connect to" << std::endl;
		}
		middleware = "zeromq";
//		ActiveModule localModule;
		ModulePort localPort;
		if(isActive(key2) && hasPort(key2.getName(),portName2)){
			ActiveModule localModule = mActiveModuleMap.at(key2);
			localPort = localModule.getModule().getPort(portName2);
		} else {
			if(mDebug){
				std::cerr << "[aimconnect] supposed local module" << key2.getName() << "is not running or it does not have the port that is suggested" << std::endl;
			}
			return false;
		}
		//Create new ModulePort that describes the other end
		std::string dataType = localPort.getType();
		ModulePort virtualPort = ModulePort(portName1,"in",dataType,middleware);

		// Create outgoing zeromq port
		myZMQports->addPort(mapPortName1,"out");

		// rename virtual port name to comply with zmq form
		std::string newPortName = "/XMPP" + mDodedodoID + "/" + mapPortName1;
		virtualPort.setName(newPortName);
		if(mDebug){
			std::cout << "[aimconnect] new outgoing port defined by: " << newPortName << std::endl;
		}

		//Connect zeromq port to local module
		std::string moduleName = key2.getName();
		std::transform(moduleName.begin(),moduleName.end(),moduleName.begin(), ::tolower);
		/* TODO: the interpretName function is now added so the modules can be properly connected.
		 * They announce themselves to the zmqserver without the git/repo/ prefix and thus can't be found
		 * with the entire git/repo/modulename this should be fixed on the level of the zmqserver, zmqconnect
		 * or default module ports
		 */
		command = "zmqconnect " + newPortName + " /" + interpretName(moduleName) + std::to_string(key2.getID()) + "/" + portName2;
		execute_command(command);

		// add to connections list
		ModuleConnection tmp = ModuleConnection(virtualPort,key1,device1,localPort,key2,device2,middleware);
		mActiveConnections.insert({mapPortName1,tmp});
		mActiveConnections.insert({mapPortName2,tmp});
		return true;
	} else {
		if(mDebug){
			std::cerr << "[aimconnect] neither module is found to be local" << std::endl;
		}
		return false;
	}
}

bool ModuleManager::aimdisconnect(std::string fullPortName){
	if(mDebug){
		std::cout << "[aim disconnect] removing connection from list, however no real connection is severed" << std::endl;
	}
	return removeConnection(fullPortName);
}

void ModuleManager::aimdataToLocal(ModuleKey key,ModulePort port,int & data){
	std::string fullPortName = makeFullPortName(key,port.getName());
	if(isConnected(fullPortName)){
		ModulePort virtualPort = mActiveConnections.at(fullPortName).getPort(fullPortName);
		ModulePort correspondingPort = mActiveConnections.at(fullPortName).getOtherPort(virtualPort);
		// check if the data types correspond
		if(correspondingPort.getType()=="int"){
			if(mDebug){
				std::cout << "[aim data(int)] sending int data to: " << key.getName();
			}
			myZMQports->writeOutGoing(correspondingPort.getName(),data);
		} else if(mDebug) {
			std::cout << "[aim data(int)] incoming data type does not match port data type" << std::endl;
		}
	} else if(mDebug) {
		std::cout << "[aim data(int)] this port is not connected" << std::endl;
	}
}

void ModuleManager::aimdataToLocal(ModuleKey key,ModulePort port,std::vector<int> & data){
	// check if the port is actually connected to the ModuleManager (XMPP)
	std::string fullPortName = makeFullPortName(key,port.getName());
	if(isConnected(fullPortName)){
		ModulePort virtualPort = mActiveConnections.at(fullPortName).getPort(fullPortName);
		ModulePort correspondingPort = mActiveConnections.at(fullPortName).getOtherPort(virtualPort);
		// check if the data types correspond
		if(correspondingPort.getType()=="intarray"){
			if(mDebug){
				std::cout << "[aim data(intarray)] sending int array data to: " << key.getName()
						<< " content size: " << data.size();
			}
			myZMQports->writeOutGoing(correspondingPort.getName(),data);
		} else if(mDebug) {
			std::cout << "[aim data(intarray)] incoming data type does not match port data type" << std::endl;
		}
	} else if(mDebug) {
		std::cout << "[aim data(intarray)] this port is not connected" << std::endl;
	}
}

void ModuleManager::aimdataToLocal(ModuleKey key,ModulePort port,float & data){
	std::string fullPortName = makeFullPortName(key,port.getName());
	if(isConnected(fullPortName)){
		ModulePort virtualPort = mActiveConnections.at(fullPortName).getPort(fullPortName);
		ModulePort correspondingPort = mActiveConnections.at(fullPortName).getOtherPort(virtualPort);
		// check if the data types correspond
		if(correspondingPort.getType()=="float"){
			if(mDebug){
				std::cout << "[aim data(float)] sending float data to: " << key.getName();
			}
			myZMQports->writeOutGoing(correspondingPort.getName(),data);
		} else if(mDebug) {
			std::cout << "[aim data(float)] incoming data type does not match port data type" << std::endl;
		}
	} else if(mDebug) {
		std::cout << "[aim data(float)] this port is not connected" << std::endl;
	}
}

void ModuleManager::aimdataToLocal(ModuleKey key,ModulePort port,std::vector<float> & data){
	// check if the port is actually connected to the ModuleManager (XMPP)
	std::string fullPortName = makeFullPortName(key,port.getName());
	if(isConnected(fullPortName)){
		ModulePort virtualPort = mActiveConnections.at(fullPortName).getPort(fullPortName);
		ModulePort correspondingPort = mActiveConnections.at(fullPortName).getOtherPort(virtualPort);
		// check if the data types correspond
		if(correspondingPort.getType()=="floatarray"){
			if(mDebug){
				std::cout << "[aim data(floatarray)] sending float data to: " << key.getName()
						<< " content size: " << data.size();
			}
			myZMQports->writeOutGoing(correspondingPort.getName(),data);
		} else if(mDebug) {
			std::cout << "[aim data(floatarray)] incoming data type does not match port data type" << std::endl;
		}
	} else if(mDebug) {
		std::cout << "[aim data(floatarray)] this port is not connected" << std::endl;
	}
}

void ModuleManager::aimdataToLocal(ModuleKey key,ModulePort port,std::string & data){
	// check if the port is actually connected to the ModuleManager (XMPP)
	std::string fullPortName = makeFullPortName(key,port.getName());
	if(isConnected(fullPortName)){
		ModulePort virtualPort = mActiveConnections.at(fullPortName).getPort(fullPortName);
		ModulePort correspondingPort = mActiveConnections.at(fullPortName).getOtherPort(virtualPort);
		// check if the data types correspond
		if(correspondingPort.getType()=="string" || correspondingPort.getType()=="std::string"){
			if(mDebug && correspondingPort.getType()=="std::string"){
				std::cout << "[aim data(string)] Warning: port type std::string is deprecated. Please update aimtools and execute aimregister again" << std::endl;
			}
			if(mDebug){
				std::cout << "[aim data(string)] sending string data to: " << key.getName()
						<< " content size: " << data.size() << std::endl;
			}
			myZMQports->writeOutGoing(correspondingPort.getName(),data);
		} else if(mDebug) {
			std::cout << "[aim data(string)] incoming data type does not match port data type" << std::endl;
		}
	} else if(mDebug) {
		std::cout << "[aim data(string)] this port is not connected" << std::endl;
	}
}

std::vector<std::string> ModuleManager::aimdataFromLocal(){
	std::vector<std::string> output;
	//list the incoming zmq ports
	std::vector<std::string> portNames = myZMQports->listIncomingPorts();
	std::string data,sendToPortName;

	//check the zmq incoming ports
	for(unsigned i=0;i<portNames.size();++i){
		data.clear();
		sendToPortName.clear();
		data = myZMQports->readIncoming(portNames[i],false);
		if(!(data=="")){
			if(mDebug){
				std::cout << "[aim data] outgoing data found" << std::endl;
			}
			//reconstruct the full portname of the source module
			std::string zmqPortName = portNames[i];
			// zmqPortname has the form:  /XMPP/devicename.modulename.id.portname
			std::string shortname = zmqPortName.substr(6);
			std::cout << "shortname = " << shortname << std::endl;
			// only use the second token (n.b. devicename.modulename.id.portname)
			std::vector<std::string> useTokens = split_string_by_char(shortname,'.');
			//TODO: check the lengths of the useToken vectors
			std::string fullPortName = makeFullPortName(useTokens[0],ModuleKey(useTokens[1],std::atoi(useTokens[2].c_str())),useTokens[3]);
			if(mDebug){
				std::cout << "[aim data] target port locally known as: " << fullPortName << std::endl;
			}

			//find the full connection description of the foreign port we are representing
			ModuleConnection connection = mActiveConnections.at(fullPortName);
			// find the corresponding Device, Module, ID & portname
			std::string targetDevice =  connection.getSinkDevice();
			ModuleKey targetKey = connection.getSinkKey();
			ModulePort targetPort = connection.getSinkPort();
			std::string targetPortName = useTokens[3];		// is the portname without /XMPP/ etc.
			std::string targetName = targetKey.getName();
			std::string targetID = std::to_string(targetKey.getID());

			// determine the type of data we are sending
			std::string dataType = targetPort.getType();

			if(mDebug){
				std::cout << "[aim data] " << dataType << " " << targetName << " " << targetID << " " << targetPortName << std::endl;
			}
			std::string message = "AIM data " + dataType + " " + targetName + " " + targetID + " " + targetPortName + " " + data;
			message = connection.getSinkDevice() + " " + message;	// add the target device name again for the xmpp messenger
			output.push_back(message);
		}
	}
	return output;
}

boost::property_tree::ptree ModuleManager::aimstatus(std::string moduleName){
	boost::property_tree::ptree pt;
	// check if active & call other aimstatus
	//if not active assign id = -1
	int id = isActive(moduleName);
	if(id==-2){ //module not present or registered on device
		pt.put("name",moduleName);
		pt.put("present on device","NO!");
	} else {
		pt = aimstatus(ModuleKey(moduleName,id));
	}
	return pt;
}

boost::property_tree::ptree ModuleManager::aimstatus(ModuleKey key){
	boost::property_tree::ptree pt,ports;
	pt.put("name",key.getName());
	pt.put("ID",std::to_string(key.getID()));
	pt.put("active",isActive(key));

	for (auto& p: mUnactiveModuleMap.at(key.getName()).getPortMap()){
		ports.push_back(std::make_pair("",aimstatus(key,p.second)));
	}
	pt.put_child("ports",ports);
	return pt;
}

boost::property_tree::ptree ModuleManager::aimstatus(ModuleKey key,ModulePort port){
	std::string fullPortName = makeFullPortName(key,port.getName());
	boost::property_tree::ptree porttree;
	porttree.put("name",port.getName());
//	if (mActiveConnections.count(port)>0){
	if (mActiveConnections.count(fullPortName)>0){
		porttree.put("connected",true);
	} else {
		porttree.put("connected",false);
	}
	return porttree;
}

void ModuleManager::aimdeploy(Module & module){
	//check if module is already on the device
//	std::string fullName = module.getName();
//	std::string shortName = interpretName(module.getName());
//	if(!isPresent(fullName) && !isPresent(shortName)){
	if(!isPresent(module.getName())){
		if(mDebug){
			std::cout << "[aim deploy] Downloading from git repository" << std::endl;
		}
		std::string command = "dodedododeploy " + module.getGit() + " " + module.getName();
		bool succes = execute_command(command);
		/*
		std::string command = "aimget " + tokens[0] + " " + module.getGit();
		execute_command(command);
		sleep(10);
		std::string location = mAimModulesLocation + "/" + tokens[0] + "/";
		// aimselect ModuleName zeromq
		execute_command("aimselect " + location + shortName + " zeromq");
		sleep(5);
		// aimmake new_module
		execute_command("aimmake " + location + shortName);
		sleep(5);
		// aimregister new_module
		execute_command("aimregister " + location + shortName);
//		execute_command("aimregister " + shortName);
		sleep(3);
		// add the module description to the Unactive Module map
		 */
		if(succes){
//			module.setName(shortName);
			module.setMiddleWare("zeromq");
			addToUnactive(module);
			if(mDebug){
				std::cout << "[aim deploy] added module:" << std::endl << module << std::endl;
			}
		} else {
			if(mDebug){
				std::cout << "[aim deploy] failed to add module:" << std::endl << module << std::endl;
			}
		}
		// (alternative to adding the module) reload the modulemap
//		loadModuleMap();
	} else {
		std::cout << "Module: " + module.getName() + " is present on this device" << std::endl;
	}
}

//=====================================================================================================================
//						NameServer Function
//=====================================================================================================================
void ModuleManager::startYarpServer(){
	if(!mYarpServRunning){	// TODO add a check that searches for the running process
		execute_command("yarp_server");
		mYarpServRunning = true;
	}
}

void ModuleManager::startZMQServer(){
	if (!mZMQServRunning){	// TODO add a check that searches for the running process
		execute_command("zmqserver");
		mZMQServRunning = true;
	}
}

void ModuleManager::stopYarpServer(){
//	TODO: improve beyond bare functionality
	execute_command("killall yarp");
	mYarpServRunning = false;
}

void ModuleManager::stopZMQServer(){
	if(mZMQServRunning){
		execute_command("killall zeromqserver");
		mZMQServRunning = false;
	}
}

void ModuleManager::ZMQInit(){
	std::cout << "Waiting for zmqserver to connect to" << std::endl;
	myZMQports->Init(mDodedodoID,mDebug);
}

std::ostream& operator<<(std::ostream& os, const ModuleManager& xec){
	os << "Available Modules: " << std::endl;
	for (auto& x: xec.mUnactiveModuleMap){
		os << x.second << std::endl;
	}
	os << "Active Modules: " << std::endl;
	for (auto& x: xec.mActiveModuleMap){
		os << x.second;
	}
	return os;
}

