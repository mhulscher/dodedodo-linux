/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file moduleconnection.cpp
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Sep 11, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#include "moduleconnection.h"
#include "line_split.h"

ModuleConnection::ModuleConnection():
	mSourceDevice("raspberrypi_0"),
	mSinkDevice("raspberrypi_0"),
	mMiddleWare("zeromq"){ }

ModuleConnection::ModuleConnection(ModulePort sourcePort,ModuleKey sourceKey,ModulePort sinkPort,ModuleKey sinkKey){
	ModuleConnection();
	mSourcePort = sourcePort;
	mSourceModuleKey = sourceKey;
	mSinkPort = sinkPort;
	mSinkModuleKey = sinkKey;
}

ModuleConnection::ModuleConnection(ModulePort sourcePort,ModuleKey sourceKey,
		ModulePort sinkPort,ModuleKey sinkKey,std::string middleware){
	ModuleConnection();
	mSourcePort = sourcePort;
	mSourceModuleKey = sourceKey;
	mSinkPort = sinkPort;
	mSinkModuleKey = sinkKey;
	mMiddleWare = middleware;
}

ModuleConnection::ModuleConnection(ModulePort sourcePort,ModuleKey sourceKey,std::string sourceDevice,ModulePort sinkPort,ModuleKey sinkKey,std::string sinkDevice,std::string middleware){
	mSourcePort = sourcePort;
	mSourceModuleKey = sourceKey;
	mSourceDevice = sourceDevice;
	mSinkPort = sinkPort;
	mSinkModuleKey = sinkKey;
	mSinkDevice = sinkDevice;
	mMiddleWare = middleware;
}

ModuleConnection::~ModuleConnection(){ }

void ModuleConnection::setMiddleWare(std::string middleware){
	mMiddleWare = middleware;
}

ModulePort ModuleConnection::getSourcePort() const{
	return mSourcePort;
}

ModulePort ModuleConnection::getSinkPort() const{
	return mSinkPort;
}

ModulePort ModuleConnection::getPort(std::string fullPortName){
	std::vector<std::string> tokens = split_string_by_char(fullPortName,'.');
	if(tokens[0]==mSourceDevice && tokens[1]==mSourceModuleKey.getName() && tokens[2]==std::to_string(mSourceModuleKey.getID()) && tokens[3]==mSourcePort.getName()){
		return mSourcePort;
	} else if(tokens[0]==mSinkDevice && tokens[1]==mSinkModuleKey.getName() && tokens[2]==std::to_string(mSinkModuleKey.getID()) && tokens[3]==mSinkPort.getName()){
		return mSinkPort;
	} else {
		return ModulePort();	// returns empty port
	}
}

ModulePort ModuleConnection::getOtherPort(ModulePort thisport){
	if(thisport.getName()==mSourcePort.getName() && thisport.getDirection()==mSourcePort.getDirection()){
		return mSinkPort;
	} else if (thisport.getName()==mSinkPort.getName() && thisport.getDirection()==mSinkPort.getDirection()){
		return mSourcePort;
	} else {
		std::cout << "Port not known to be connected here" << std::endl;
		return ModulePort();	// empty port
	}
}

ModuleKey ModuleConnection::getOtherModuleKey(ModulePort thisport){
	if(thisport.getName()==mSourcePort.getName() && thisport.getDirection()==mSourcePort.getDirection()){
		return mSinkModuleKey;
	} else if (thisport.getName()==mSinkPort.getName() && thisport.getDirection()==mSinkPort.getDirection()){
		return mSourceModuleKey;
	} else {
		std::cout << "Port not known to be connected here" << std::endl;
		return ModuleKey();	// empty key
	}
}

ModuleKey ModuleConnection::getThisModuleKey(ModulePort thisport){
	if(thisport.getName()==mSourcePort.getName() && thisport.getDirection()==mSourcePort.getDirection()){
		return mSourceModuleKey;
	} else if (thisport.getName()==mSinkPort.getName() && thisport.getDirection()==mSinkPort.getDirection()){
		return mSinkModuleKey;
	} else {
		std::cout << "Port not known to be connected here" << std::endl;
		return ModuleKey();	// empty key
	}
}

ModuleKey ModuleConnection::getSinkKey(){
	return mSinkModuleKey;
}

std::string ModuleConnection::getOtherFullPortName(std::string fullPortName){
	std::string firstName = mSourceDevice + "." + mSourceModuleKey.getName() + "." +
			std::to_string(mSourceModuleKey.getID()) + "." + mSourcePort.getName();
	std::string secondName = mSinkDevice + "." + mSinkModuleKey.getName() + "." +
			std::to_string(mSinkModuleKey.getID()) + "." + mSinkPort.getName();
	if(fullPortName == firstName){
		return secondName;
	} else if(fullPortName==secondName){
		return firstName;
	}
	return "";
}

std::string ModuleConnection::getSourceDevice() const{
	return mSourceDevice;
}

std::string ModuleConnection::getSinkDevice() const{
	return mSinkDevice;
}
